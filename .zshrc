# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/david/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

alias ls='ls --color=auto'
alias install='sudo pacman -S'
alias update='sudo pacman -Syu'
alias yinstall='yaourt -S'
alias yupdate='yaourt -Syu'
alias remove='sudo pacman -Rns'
alias ll='ls -la'
alias img='feh -q -. -d'
alias imgf='img -F'

bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line
bindkey "${terminfo[kdch1]}" delete-char

# tab-complition
autoload -U compinit
compinit
zstyle ':completion:*:default' menu select=0