#!/bin/sh

i3status | (read line && echo "$line" && read line && echo "$line" && read line && echo "$line" && while :
do
	read line
	json="{\"full_text\":\"\" }"

	# Brightness
	#json="{\"full_text\":\" `~/.config/i3/BrightnessNotify.sh get`%\" }"

	ACPI=`acpi -b`
	BatteryStatus=$(echo $ACPI | awk -F'[,:%]' '{print $2}')
	BatteryLevel=$(echo $ACPI | awk -F'[,:%]' '{print $3}')
	BatteryTime=$(echo $ACPI | sed -e 's,^.*\%\, [0-9]\([0-9]:[0-9][0-9]\).*,\1,')
	#BatteryTime=$(echo $ACPI | sed -e 's,^.*\%\, \([0-9][0-9]:[0-9][0-9]:[0-9][0-9]\).*,\1,')

    #artist=$(cmus-remote -Q | grep ' artist ' | cut -d ' ' -f3-)
    song=$(cmus-remote -Q | grep title | cut -d ' ' -f3-)
    if [ "$song" == "" ]; then
    	song=""
    else 
    	songStatus=$(cmus-remote -Q | grep status | cut -d ' ' -f2-)
    	if [ "$songStatus" == "playing" ]; then
    		song="   $song"
    	else
    		song="   $song"
    	fi
    fi

    # Song
    if [ "$song" != "" ]; then
		json="$json,{\"full_text\":\"$song\" }"
    fi

    # Volume
	Volume=`~/.config/i3/VolumeNotify.sh get`
    if [ "$Volume" == "mute" ]; then
    	Volume=" "
    else
		if [ "`~/.config/i3/VolumeNotify.sh get dev`" == "Built-in Audio Analog" ]; then
			Volume=" $Volume%"
		else
			Volume=" $Volume%"
		fi
	fi
	json="$json,{\"full_text\":\"$Volume\" }"


	if [ "$BatteryLevel" -lt "10" ]; then
		Battery="" #empty
	elif [ "$BatteryLevel" -lt "25" ]; then
		Battery="" #quater
	elif [ "$BatteryLevel" -lt "50" ]; then
		Battery="" #half
	elif [ "$BatteryLevel" -lt "75" ]; then
		Battery="" #3quater
	else
		Battery=""
	fi
	if [ "$BatteryStatus" == " Charging" ];	then
		Battery="$Battery   $BatteryTime"
	elif [ "$BatteryStatus" == " Discharging" ]; then
		Battery="$Battery  $BatteryTime"
	elif [ "$BatteryStatus" == " Full" ]; then
		Battery="$Battery  Full"
	else
		Battery="$Battery  -:-"
	fi
	#Battery="$Battery $BatteryLevel% $BatteryTime"

	if [ "$BatteryLevel" -lt "10" ] && [ "$BatteryStatus" == " Discharging" ]; then
		json="$json,{\"full_text\":\"$Battery\", \"color\":\"#ff0000\" }"
	elif [ "$BatteryLevel" -lt "30" ] && [ "$BatteryStatus" == " Discharging" ]; then
		json="$json,{\"full_text\":\"$Battery\", \"color\":\"#ffa500\" }"
	else
		json="$json,{\"full_text\":\"$Battery\", \"color\":\"#ffffff\" }"
	fi
	echo ",[$json,${line#,\[}" || exit 1
done)
