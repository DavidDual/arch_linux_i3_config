#!/bin/bash
# Brightnes notification

xbacklight $1 $2

Brightness=$(xbacklight | sed -e 's,^\(.*\)\..*,\1,')
if [ "$1" == "get" ]; then
	echo $Brightness
else
	ID=$(cat ~/.config/i3/.dunst_brightnes)
	if [ $ID -gt "0" ]
	then
	 	dunstify -p -r $ID " $Brightness%" > ~/.config/i3/.dunst_brightnes
	else
	 	dunstify -p " $Brightness%" > ~/.config/i3/.dunst_brightnes
	fi
fi
