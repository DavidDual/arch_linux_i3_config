#!/bin/bash
# Volume notification

#notify-send "Volume" `pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,'`

sources=$(pactl list sinks | grep 'Description')
count=$(echo $sources | grep -n  $(head -n 1 ~/.config/i3/audio_output) | grep -o "[0-9]\+" | head -n 1)
if [ -z "$count" ]
then
	count=$(pactl list sinks | grep '^Sink #' | wc -l)
fi
sound_out=$(pactl list sinks | grep '^Sink #' | sed -e 's/^Sink #\(.*\)$/\1/1' | sed $count'!d')

if [ "$1" == "-" ]
then
 	pactl set-sink-mute $sound_out 0
 	pactl set-sink-volume $sound_out -$2% 
 	ICON="/usr/share/icons/Adwaita/16x16/status/audio-volume-low.png"
elif [ "$1" == "+" ]
then
 	pactl set-sink-mute $sound_out 0
 	pactl set-sink-volume $sound_out +$2% 
 	ICON="/usr/share/icons/Adwaita/16x16/status/audio-volume-high.png"
elif [ "$1" == "mute" ]
then
 	pactl set-sink-mute $sound_out toggle
 	ICON="/usr/share/icons/Adwaita/16x16/status/audio-volume-muted.png"
fi

Volume=$(pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + count )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')
MUTE=$(pactl list sinks | grep '^[[:space:]]Mute:' | head -n $(( $SINK + count )) | tail -c 2)

if [ "$1" == "get" ]; then
	if [ "$2" == "dev" ]; then
		echo $(pactl list sinks | grep '^[[:space:]]Description:' | sed -e 's,.*: \(.*\) .*,\1,')
		exit
	fi
	if [ "$MUTE" == "s" ]; then
		echo "mute"
		exit
	fi
	echo $Volume
elif [ "$1" == "swich" ]
then
 	pactl set-sink-mute $sound_out toggle
 	ICON="/usr/share/icons/Adwaita/16x16/status/audio-volume-muted.png"
else
	if [ "$MUTE" == "s" ]; then
		TEXT="  mute"
	else
		TEXT=" $Volume%"
	fi
	ID=$(cat ~/.config/i3/.dunst_volume)
	if [ $ID -gt "0" ]
	then
	 	dunstify -p -r $ID "$TEXT" > ~/.config/i3/.dunst_volume
	else
	 	dunstify -p "$TEXT" > ~/.config/i3/.dunst_volume
	fi
fi