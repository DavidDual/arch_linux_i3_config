#!/bin/sh

ID=`xinput list | grep 'Synaptics' | sed -e 's/^\(.*\)id=\(.*\)\t\(.*\)$/\2/1'`
Scrolling=`xinput list-props $ID | grep 'Natural Scrolling Enabled (' | sed -e 's/^\(.*\)(\(.*\))\(.*\)$/\2/1'`
xinput set-prop $ID $Scrolling 1
Tapping=`xinput list-props $ID | grep 'Tapping Enabled (' | sed -e 's/^\(.*\)(\(.*\))\(.*\)$/\2/1'`
xinput set-prop $ID $Tapping 1

